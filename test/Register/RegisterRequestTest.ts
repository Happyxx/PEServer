/**
 * Created by Weizehua on 2017/1/27.
 */

import {suite, test, skip} from "mocha-typescript";
import {Request} from "../Core/Request";
import {assertHandlerSuccess, assertHandlerFail} from "../Core/Utils";
import {throws} from "assert";

@suite
export class RegisterRequestTest {
    @test
    async '/register/phone/getCaptcha'(){
        let phone = String(Math.floor(Math.random() * 9e10 + 1e10));
        let res = await Request.get("/register/phone/getCaptcha", {
            phone: phone,
        });
        assertHandlerSuccess(res);
    }

    @test
    async '/register/phone/getCaptcha - compatible type conversion'(){
        let phone = Number(Math.floor(Math.random() * 9e10 + 1e10));
        let res = await Request.get("/register/phone/getCaptcha", {
            phone: phone,
        });
        assertHandlerSuccess(res);
    }

    @test
    async '/register/phone/getCaptcha - fail(anti-x): too long'(){
        let phone = String(Math.floor(Math.random() * 9e10 + 1e10)) + '1';
        let res = await Request.get("/register/phone/getCaptcha", {
            phone: phone,
        });
        assertHandlerFail(res);
    }

    @test
    async '/register/phone/getCaptcha - fail(anti-x): too short'(){
        let phone = String(Math.floor(Math.random() * 9e9 + 1e9));
        let res = await Request.get("/register/phone/getCaptcha", {
            phone: phone,
        });
        assertHandlerFail(res);
    }

    @test
    async '/register/phone/getCaptcha - fail(anti-x): no phone'(){
        let res = await Request.get("/register/phone/getCaptcha", {
        });
        assertHandlerFail(res);
    }
}
