/**
 * Created by Weizehua on 2017/1/27.
 */
import {RegisterRequestTest} from "./Register/RegisterRequestTest";
import {NewImageRequestTest} from "./Image/NewImageRequestTest";
import {AIO_RegisterLogin} from "./AllInOne/RegisterLogin/AIO_RegisterLogin";
import {QueryImageRequestTest} from "./Image/QueryImageRequestTest";
import {AlbumTest} from "./Album/Album";
import {AddSpotRequestTest} from "./Image/AddSpotRequestTest";


export function loadAllTests(){
    // just reference all test, they will be run automatically
    let t;
    t = RegisterRequestTest;
    t = NewImageRequestTest;
    t = AIO_RegisterLogin;
    t = QueryImageRequestTest;
    t = AlbumTest;
    t = AddSpotRequestTest;
}

