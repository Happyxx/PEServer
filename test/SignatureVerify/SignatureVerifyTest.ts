/**
 * Created by Weizehua on 2017/2/11.
 */
import {test} from "mocha-typescript";
import {assertHandlerFail} from "../Core/Utils";
import {Request} from "../core/request";
import {generateUploadInfoFromUrl} from "../Core/UploadInfoGenerator";

export class SignatureVerifyTest {
    @test
    async '/album/new - no signature: should not pass'() {
        // new album
        let title = 'title';
        let imageSource = 'http://peres.oss-cn-shenzhen.aliyuncs.com/test/test-album-cover.svg';
        let res = await Request.userGet('/album/new', {
            title: title,
            uploadInfo: await generateUploadInfoFromUrl(imageSource),
        });
        assertHandlerFail(res);
    }

}

