/**
 * Created by Weizehua on 2017/1/29.
 */

import {suite, test, skip} from "mocha-typescript";
import {Request} from "../Core/Request";
import {assertHandlerSuccess} from "../Core/Utils";
import {generateUploadInfoFromUrl} from "../Core/UploadInfoGenerator";
import {generateSign} from "../Core/SignatureGenerator";
@suite
export class AlbumTest {
    @test
    async '/album/new - message: should pass'(){
        // new album
        let title = 'title';
        let imageSource = 'http://peres.oss-cn-shenzhen.aliyuncs.com/test/test-album-cover.svg';
        let params = {
            title: title,
            uploadInfo: await generateUploadInfoFromUrl(imageSource),
        };
        let res = await Request.userGet('/album/new', await generateSign(['uploadInfo'], params));
        assertHandlerSuccess(res);
    }

    @test
    @skip
    async '/album/query/all - message: should pass'(){
        let res = await Request.userGet('/album/new');
        assertHandlerSuccess(res);
    }
}
