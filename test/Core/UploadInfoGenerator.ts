import {Injector} from "ts.di";
import {Hash} from "../../src/Core/Hash/Hash";
import {AliOSS} from "../../AliOSS/AliOSS";
/**
 * Created by Weizehua on 2017/1/29.
 */

export async function generateUploadInfoFromUrl(url:string) {
    let signature = await Injector.get(Hash).sign(url);
    return {url: url};
}
export async function generateUploadInfoFromPath(path:string) {
    let url = await Injector.get(AliOSS).path2url(path);
    return {url: url};
}
