/**
 * Created by Weizehua on 2017/1/27.
 */
import {Options} from "request-promise";
import request = require("request-promise");
import fs = require("fs")
import {RequestUserInfo} from "../../src/Core/Dispatcher/Types";
import {HostInfo} from "../../src/https_server";
import {generateUser} from "./UserGenereator";

export class Request {
    static ca = fs.readFileSync('ca.crt');
    static async userGet(path: string, param: any = {}) {
        let userInfo = await generateUser();
        return await this.get(path, param, userInfo);
    }

    static async get(path: string, param: any = {}, userInfo?: RequestUserInfo) {
        let options: Options = {
            method: 'POST',
            url: 'https://localhost:1234' + path,
            body: {
                params: param,
                userInfo: userInfo
            },
            json: true,
            ca: Request.ca
        };
        return await request.post('https://localhost:1234' + path, options);
    }
}
