/**
 * Created by Weizehua on 2017/1/28.
 */
import {RequestUserInfo} from "../../src/Core/Dispatcher/Types";
import {Hash} from "../../src/Core/Hash/Hash";
import {Injector} from "ts.di";
import {User} from "../../src/User/User";
import {UserService} from "../../src/User/UserService";

export async function generateUser(): Promise<RequestUserInfo> {
    let hasher = Injector.get(Hash);

    let id = 0;
    let permissions = {user: true};
    let expiredDate = Number(new Date()) + 1000 * 3600 * 10;
    let signature = await hasher.signUser(id, permissions, expiredDate);

    return {
        id: id,
        permissions: permissions,
        expiredDate: expiredDate,
        signature: signature
    }
}

export async function generateRealUser(user: User) : Promise<RequestUserInfo> {
    return Injector.get(UserService).generateUserInfo(user);
}
