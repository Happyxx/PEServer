/**
 * Created by Weizehua on 2017/2/11.
 */
import {Injector} from "ts.di";
import {Dispatcher} from "../../src/Core/Dispatcher/Dispatcher";
import {access} from "../../src/Utils/ObjectOperate/ObjectOperate";

export async function generateSign(paths: string[], obj: any) {
    let dispatcher:any = Injector.get(Dispatcher) as any;
    for(let path of paths) {
        let val = access(obj, path);
        if(val instanceof Array) {
            for(let v of val) {
                v.signature = await dispatcher.sign(v, true);
            }
        }
        else {
            val.signature = await dispatcher.sign(val, true);
        }
    }
    return obj;
}

