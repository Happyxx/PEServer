/**
 * Created by Weizehua on 2017/1/27.
 */

import {FormalHandlerResponse} from "../../src/Core/Dispatcher/Types";
import {equal} from "assert";

export function assertHandlerSuccess(result: FormalHandlerResponse) {
    equal(result.success, true, result.reason);
}

export function assertHandlerFail(result: FormalHandlerResponse) {
    equal(result.success, false, result.reason);
}
