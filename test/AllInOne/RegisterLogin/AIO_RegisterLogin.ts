/**
 * Created by Weizehua on 2017/1/28.
 */

import {suite, test, skip} from "mocha-typescript";
import {Request} from "../../Core/Request";
import {equal} from "assert";
import {isLogged, UserHandler} from "../../../src/User/UserHandler";
import {Injector} from "ts.di";
@suite('All in One: register and login then check.')
export class AIO_RegisterLogin{
    @test
    async 'AIO: register and login then check logged with isLogged() - should pass' () {
        let phone = String(Math.floor(1e10 + Math.random() * 1e9));
        let password = "test password" + Math.random();
        // get captcha
        let captchaInfo = await Request.get('/register/phone/getCaptcha', {phone: phone});
        equal(captchaInfo.success, true, captchaInfo.reason);

        let registerResult = await Request.get('/register/phone',
            {phone: phone, captchaInfo: captchaInfo.captchaInfo, captcha: '123456', password: password});
        equal(registerResult.success, true, registerResult.reason);

        let loginResult = await Request.get('/login', {username: phone, password: password});
        equal(loginResult.success, true, loginResult.reason);

        let userInfo = loginResult.userInfo;
        let checkResult = await Injector.get(UserHandler).isLogged(userInfo);
        equal(checkResult, true, "Login check using isLogged() failed!");
    }
}
