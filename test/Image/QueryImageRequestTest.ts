/**
 * Created by Weizehua on 2017/1/28.
 */
import {suite, test} from "mocha-typescript";
import {Request} from "../Core/Request";
import {assertHandlerSuccess} from "../Core/Utils";
import {equal} from "assert";
@suite
export class QueryImageRequestTest {
    @test
    async '/image/queryByUser/newer - message: should pass'() {
        let res = await Request.userGet('/image/queryByUser/newer', {userId: 0, endId: 0});
        assertHandlerSuccess(res);
    }
}
