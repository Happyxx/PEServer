/**
 * Created by Weizehua on 2017/2/12.
 */
import {suite, test, skip} from "mocha-typescript";
import {User} from "../../src/User/User";
import {Album} from "../../src/Album/Album";
import {Injector} from "ts.di";
import {Connection} from "typeorm";
import {equal} from "assert";
import {Image} from "../../src/image/image";
import {Request} from "../Core/Request";
import {generateSign} from "../Core/SignatureGenerator";
import {assertHandlerSuccess} from "../Core/Utils";
import {generateUser, generateRealUser} from "../Core/UserGenereator";

@suite
export class AddSpotRequestTest {
    static user: User;
    static album: Album;
    static image: Image;

    static async before() {
        // insert user
        let typeorm = Injector.get(Connection);
        let u = new User();
        u.username = "test" + Math.random();
        AddSpotRequestTest.user = await typeorm.entityManager.persist(u);
        equal(!!AddSpotRequestTest.user, true);

        // insert album
        let a = new Album();
        a.imageSource = "";
        a.title = "";
        AddSpotRequestTest.album = await typeorm.entityManager.persist(a);
        equal(!!AddSpotRequestTest.album, true);

        // insert image
        let i = new Image();
        i.album = a;
        i.imageSource = "123";
        i.spots = [];
        i.user = u;
        AddSpotRequestTest.image = await typeorm.entityManager.persist(i);
        equal(!!AddSpotRequestTest.image, true);
    }

    static async after() {
        let typeorm = Injector.get(Connection);
        await typeorm.entityManager.remove(AddSpotRequestTest.user);
        // album, image, spots will be removed automatically
    }

    @test
    async '/image/spot/add (other user) - message : should pass'() {
        let s1: any = {};
        s1.image = {
            id: AddSpotRequestTest.image.id,
            user: AddSpotRequestTest.image.user.id,
            album: AddSpotRequestTest.image.album.id,
        };
        s1.x = .5;
        s1.y = .6;
        s1.title = "title";
        s1.detail = "detail";

        let params = {
            image: s1.image,
            spot: s1
        };
        let res = await Request.get('/image/spot/add', await generateSign(['image'], params),
            await generateUser());
        assertHandlerSuccess(res);
    }

    @test
    @skip
    async '/image/spot/add (self) - message : should pass'() {
        let s1: any = {};
        s1.image = {
            id: AddSpotRequestTest.image.id,
            user: AddSpotRequestTest.image.user.id,
            album: AddSpotRequestTest.image.album.id,
        };
        s1.x = .5;
        s1.y = .6;
        s1.title = "title";
        s1.detail = "detail";

        let params = {
            image: s1.image,
            spot: s1
        };
        let res = await Request.get('/image/spot/add', await generateSign(['image'], params),
            await generateRealUser(AddSpotRequestTest.user));
        assertHandlerSuccess(res);
    }
}
