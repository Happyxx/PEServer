/**
 * Created by Weizehua on 2017/3/9.
 */

import fs = require("fs")
import {ServerOptions} from "https";
import {ConnectionOptions} from "typeorm";

export let adminUserPhones = {"18807734861": true};
export let serverPort = 1234;

// ssl settings
export let serverOptions = <ServerOptions>{
        ca: fs.readFileSync("ca.crt"),
        cert: fs.readFileSync("server_happyzh_com.crt"),
        key: fs.readFileSync("server.key"),
        passphrase: "0403",
        //requestCert : true,
        secureProtocol: "TLSv1_2_method",
    };

// mysql settings
export let typeormOptions: ConnectionOptions = {
    driver: {
        type: "mysql",
        host: "localhost",
        username: "pe",
        password: "peUserPassword",
        database: "pe"
    },
    logging: {
        logQueries: true
    }
};
