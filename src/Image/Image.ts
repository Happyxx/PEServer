/**
 * Created by Weizehua on 2017/1/26.
 */
import {PETable} from "../Core/Database/Database";
import {PrimaryGeneratedColumn, Column, CreateDateColumn, Index, OneToMany, ManyToOne, JoinColumn} from "typeorm";
import {User} from "../User/User";
import {Spot} from "./Spot";
import {Album} from "../Album/Album";
@PETable()
@Index((image: Image) => [image.id, image.creationDate])
@Index((image: Image) => [image.user])
@Index((image: Image) => [image.user, image.creationDate])
@Index((image: Image) => [image.creationDate])
export class Image {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    imageSource: string;

    @CreateDateColumn()
    creationDate: Date;

    @OneToMany(type => Spot, (spot: Spot) => spot.image)
    spots: Spot[];

    @ManyToOne(type => User, (user: User) => user.images, {onDelete: 'CASCADE'})
    @JoinColumn()
    user: User;

    @ManyToOne(type => Album, (album: Album) => album.images, {onDelete: 'CASCADE'})
    @JoinColumn()
    album: Album;
}
