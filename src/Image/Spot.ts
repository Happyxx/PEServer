/**
 * Created by Weizehua on 2017/1/27.
 */
import {PETable} from "../Core/Database/Database";
import {PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, Index} from "typeorm";
import {Image} from "./Image";
import {User} from "../User/User";
@PETable()
@Index((spot: Spot) => [spot.image])
@Index((spot: Spot) => [spot.image, spot.creationDate])
export class Spot {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    x: number;

    @Column()
    y: number;

    @Column({length: 10})
    title: string;

    @Column({length: 30})
    detail: string;

    @ManyToOne(type => Image, (image: Image) => image.spots, {onDelete: 'CASCADE'})
    image: Image;

    @ManyToOne(type => User, (user: User) => user.spots, {onDelete: 'CASCADE'})
    user: User;

    @CreateDateColumn()
    creationDate: Date;
}
