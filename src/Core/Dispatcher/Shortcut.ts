/**
 * Created by Weizehua on 2017/1/29.
 */
import {Verify, Require} from "./Dispatcher";
import {isLogged} from "../../User/UserHandler";

export function RequireLogin(): MethodDecorator {
    return Require(isLogged);
}
export function VerifyUpload(): MethodDecorator {
    return Verify('uploadInfo', {url: true});
}
