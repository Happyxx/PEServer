/**
 * Created by Weizehua on 2017/1/17.
 */
import {Singleton, Injector} from "ts.di";
export function tr(s: string|TemplateStringsArray, ...v: any[]) {
    if (s instanceof Array) {
        return Injector.get(Translator).translateRaw(<TemplateStringsArray>s, ...v);
    }
    return Injector.get(Translator).translate(<string>s);
}

enum Locale {
    EN,
    CN
}

@Singleton()
class Translator {
    translate(s: string) {
        if (this._locale != this._defaultLocale) {
            throw new Error(`no translate for locale : ${this._locale}`);
        }
        return s;
    }

    translateRaw(s: TemplateStringsArray, ...v: string[]) {
        if (this._locale != this._defaultLocale) {
            throw new Error(`no translate for locale : ${this._locale}`);
        }
        return String.raw(s, ...v);
    }

    get locale(): Locale {
        return this._locale;
    }

    private _locale = Locale.EN;
    private _defaultLocale = Locale.EN;
}
