import {tr} from "../Localization/Translator";
/**
 * Created by Weizehua on 2017/1/28.
 */

export function MinLen(len: number) {
    return (str: string) => {
        if (str.length < len)
            return tr(`${str}:长度不够，最少${len}位。`);
        return true;
    }
}

export function ExactLen(len: number) {
    return (str: string) => {
        if (str.length !== len)
            return tr(`${str}:长度不符，应为${len}位。`);
        return true;
    }
}

export function MaxLen(len: number) {
    return (str: string) => {
        if (str.length > len)
            return tr(`${str}:长度过长，最多${len}位。`);
        return true;
    }
}

