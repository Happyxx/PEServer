/**
 * Created by Weizehua on 2017/2/19.
 */
import {PETable} from "../Core/Database/Database";
import {OneToOne, Column, JoinColumn, PrimaryGeneratedColumn} from "typeorm";
import {User} from "../User/User";
@PETable()
export class UserInfo {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => User, (user: User) => user.information, {onDelete: 'CASCADE'})
    @JoinColumn()
    user: User;

    @Column()
    nickname: string = '无名';

    @Column()
    headUrl: string = 'assets/image/no-avatar-01.png';
}
