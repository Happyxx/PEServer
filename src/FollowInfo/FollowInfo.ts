/**
 * Created by Weizehua on 2017/2/19.
 */
import {PETable} from "../Core/Database/Database";
import {ManyToOne, PrimaryGeneratedColumn, Index} from "typeorm";
import {User} from "../User/User";

@PETable()
@Index((info: FollowInfo) => [info.user, info.follower], {unique: true})
export class FollowInfo {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, (user: User) => user.followInfo, {onDelete: 'CASCADE'})
    user: User;

    @ManyToOne(type => User, (user: User) => user.followerInfo, {onDelete: 'CASCADE'})
    follower: User;
}
