/**
 * Created by Weizehua on 2017/1/24.
 */
import {Column, Index, PrimaryGeneratedColumn, CreateDateColumn} from "typeorm";
import {PETable} from "../Core/Database/Database";

@PETable()
@Index((info: SMSSentInfo) => [info.phone, info.sentDate])
export class SMSSentInfo {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 11})
    phone: string;

    @Column({length: 10})
    captcha: string;

    @CreateDateColumn()
    sentDate: Date;
}
