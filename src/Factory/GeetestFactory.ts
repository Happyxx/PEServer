/**
 * Created by Weizehua on 2017/1/25.
 */
import {Singleton, FactorOf} from "ts.di";
import {Geetest} from "../../Geetest/Geetest";

@Singleton()
export class GeetestFactory {
    @FactorOf(Geetest)
    static createGeetest(): Geetest {
        return new Geetest(
            {
                geetest_id: '7c25da6fe21944cfe507d2f9876775a9',
                geetest_key: 'f5883f4ee3bd4fa8caec67941de1b903'
            });
        // return new GeetestService({
        //     geetest_id: '81e571cfbe5143a210b76ee368146069',
        //     geetest_key: 'c25f340a6d14e05e631739565d544c93'
        // });
    }
}

