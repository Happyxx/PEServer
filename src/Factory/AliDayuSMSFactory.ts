/**
 * Created by Weizehua on 2017/1/25.
 */
import {Singleton, FactorOf} from "ts.di";
import {AliDayuSMS} from "../../AliDayuSMS/AliDayuSMS";
@Singleton()
export class AliDayuSMSFactory {
    @FactorOf(AliDayuSMS)
    static createAliDayuSMS() {
        return new AliDayuSMS('23613667', 'd26498c5e415b7c5c98f82cdb4562ba5', '画街');
    }
}
