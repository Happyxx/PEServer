/**
 * Created by Weizehua on 2017/1/25.
 */
import {Singleton, FactorOf} from "ts.di";
import {Hash} from "../Core/Hash/Hash";

@Singleton()
export class HasherFacotry {
    @FactorOf(Hash)
    static createHash() {
        return new Hash("gna3UAIvV6DyPJ96UU5tgR6B9tjr0m3ixqVIzmhrnXa0b3yK2ZSDd1VvwLCxUNsLZkEUsuDC0hXwYcPpF0fDyNx4vfOpfsyE9qe9mah16nsC+9XajKRCgeWg/cHfak7xFphe0ioKzPuM/+xVSSy4IbvOSMdJaJ7E5SVBgJvzPeQ=",
            "T3bfFJBdTYWdZIwf8QQs9xYBsL1FJL/MYwNhtKwJEXVZODukJk6XQZJVunFOMbkPCKgRwjsvG9ZJHavcK1dXTm8qjuHkQKPG1euTyDIJcjXkO1pPOZ9ay5KTwpclN+QNk18BR0MFRL3MmR1pkl/5+U7snISvX9yJDk4d15ApBTc=",
            '3buj7Fn9yJUgEZfQYcN1izxzvks6SHiefHE650QZDc9/kSmmhHPPL30X7BWYQJIA9Q0aYgIrd/bC0F7B8p2ErTDtp7lUpKm8fsV3yGWbIsOEzA4WiQdQQr+hRhRecebfzI0iFsce9WJjtLePEw4Td5ahwT86rGpiVbBTj5WS9TQ=');
    }
}
