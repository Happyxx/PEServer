/**
 * Created by Weizehua on 2017/1/13.
 */
import {Main} from "./Process";
import * as process from "process";
import {logger} from "./Core/Logger/Logger";

process.on("unhandledRejection", (reason, p) => {
    logger.error(p);
});

let main = new Main();
main.exec().catch(errorHandler);

function errorHandler(e: Error) {
    logger.error(`!!!Error raised to root: ${e.name} : ${e.message}
stack trace: ${e.stack}`);
}
