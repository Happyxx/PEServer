/**
 * Created by Weizehua on 2017/1/29.
 */
import {PETable} from "../Core/Database/Database";
import {PrimaryGeneratedColumn, Column, CreateDateColumn, Index, ManyToOne, OneToMany, OneToOne} from "typeorm";
import {User} from "../User/User";
import {Image} from "../Image/Image";
import {WeeklyHeat, DailyHeat, MonthlyHeat} from "./AlbumsHeat";
@PETable()
@Index((album: Album) => [album.title])
export class Album {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 20})
    title: string;

    @Column()
    imageSource: string;

    @CreateDateColumn()
    creationDate: Date;

    @OneToOne(type => DailyHeat, (heat: DailyHeat) => heat.album, {cascadeInsert: true})
    dailyHeat: DailyHeat;

    @OneToOne(type => WeeklyHeat, (heat: WeeklyHeat) => heat.album, {cascadeInsert: true})
    weeklyHeat: any;

    @OneToOne(type => MonthlyHeat, (heat: MonthlyHeat) => heat.album, {cascadeInsert: true})
    monthlyHeat: any;

    @OneToMany(type => Image, (image: Image) => image.album)
    images: Image[];

    @ManyToOne(type => User, (user: User) => user.albums, {onDelete: 'CASCADE'})
    user: User;
}

