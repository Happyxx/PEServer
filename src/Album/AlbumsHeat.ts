/**
 * Created by Weizehua on 2017/1/30.
 */
import {Album} from "./Album";
import {OneToOne, Column, JoinColumn, Index} from "typeorm";
import {PETable, PEAbstractTable} from "../Core/Database/Database";

@PEAbstractTable()
@Index((top: AlbumsHeat) => [top.heat])
export class AlbumsHeat {
    @Column({type: 'int', default: '0'})
    heat: number;

    @Column({type: 'boolean', default: false})
    isRecommended: boolean = false;

    @Column()
    recommendDate: Date = new Date();
}

@PETable('DailyHeat')
export class DailyHeat extends AlbumsHeat {
    @OneToOne(type => Album, (album: Album) => album.dailyHeat, {primary: true, onDelete: 'CASCADE'})
    @JoinColumn()
    album: Album;
}

@PETable('WeeklyHeat')
export class WeeklyHeat extends AlbumsHeat {
    @OneToOne(type => Album, (album: Album) => album.weeklyHeat, {primary: true, onDelete: 'CASCADE'})
    @JoinColumn()
    album: Album;
}

@PETable('MonthlyHeat')
export class MonthlyHeat extends AlbumsHeat {
    @OneToOne(type => Album, (album: Album) => album.monthlyHeat, {primary: true, onDelete: 'CASCADE'})
    @JoinColumn()
    album: Album;
}

export type AlbumsHeats =  typeof DailyHeat | typeof WeeklyHeat | typeof MonthlyHeat;
