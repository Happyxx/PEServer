/**
 * Created by Weizehua on 2017/1/18.
 */
import {PrimaryGeneratedColumn, Column, CreateDateColumn, Index, OneToMany, OneToOne} from "typeorm";
import {PETable} from "../Core/Database/Database";
import {Image} from "../Image/Image";
import {Album} from "../Album/Album";
import {Permission} from "./Permission";
import {Spot} from "../Image/Spot";
import {FollowInfo} from "../FollowInfo/FollowInfo";
import {UserInfo} from "../UserInfo/UserInfo";

@PETable()
@Index((user: User) => [user.username, user.phone, user.email], {unique: true})
@Index((user: User) => [user.username, user.password])
@Index((user: User) => [user.phone, user.password])
@Index((user: User) => [user.email, user.password])
@Index((user: User) => [user.creationDate])
@Index((user: User) => [user.lastAccess])
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 30, default: "NULL"})
    username: string;

    @Column({length: 11, default: "NULL"})
    phone: string;

    @Column({length: 30, default: "NULL"})
    email: string;

    @Column({length: 256, default: "NULL"})
    password: string;

    @CreateDateColumn()
    creationDate: Date;

    @Column()
    lastAccess: Date = new Date();

    @OneToOne(type => UserInfo, (userInfo: UserInfo) => userInfo.user, {cascadeInsert: true})
    information: UserInfo;

    @OneToMany(type => Image, (image: Image) => image.user)
    images: Image[];

    @OneToMany(type => Album, (album: Album) => album.user)
    albums: Album[];

    @OneToMany(type => Permission, (p: Permission) => p.user)
    permissions: Permission[];

    @OneToMany(type => Spot, (spot: Spot) => spot.user)
    spots: Spot[];

    @OneToMany(type => FollowInfo, (followInfo: FollowInfo) => followInfo.user, {cascadeInsert: true})
    followInfo: FollowInfo;

    @OneToMany(type => FollowInfo, (followInfo: FollowInfo) => followInfo.follower, {cascadeInsert: true})
    followerInfo: FollowInfo;
}
