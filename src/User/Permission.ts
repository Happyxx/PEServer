import {OneToOne, Index, CreateDateColumn, Column, PrimaryGeneratedColumn, JoinColumn} from "typeorm";
import {PETable} from "../Core/Database/Database";
import {User} from "./User";
/**
 * Created by Weizehua on 2017/1/19.
 */

export enum PermissionFlag {
    tourist = 1,
    user = 2,
    admin = 3,
    root = 4,
}

export interface PermissionInfo {
    user?: boolean;
    tourist?: boolean;
    admin?: boolean;
    root?: boolean;
}

@PETable()
@Index((permission: Permission) => [permission.user, permission.permission], {unique: true})
@Index((permission: Permission) => [permission.creationDate])
export class Permission {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => User, {onDelete: 'CASCADE'})
    @JoinColumn()
    user: User;

    @Column({type: 'int', length: 2})
    permission: PermissionFlag;

    @CreateDateColumn()
    creationDate: Date;
}

