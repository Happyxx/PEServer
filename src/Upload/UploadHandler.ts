/**
 * Created by Weizehua on 2017/1/25.
 */
import {HandlerClass, Handle, ParamType, Sign} from "../Core/Dispatcher/Dispatcher";
import {AliOSS} from "../../AliOSS/AliOSS";
import {Hash} from "../Core/Hash/Hash";

@HandlerClass()
export class UploadHandler {
    constructor(public alioss: AliOSS, public hasher: Hash) {
    }

    @Handle('/upload/prepare')
    @ParamType({md5: String, size: Number})
    @Sign(".", {url: true})
    async uploadPrepareHandler(param: any) {
        let {path, headers} = await this.alioss.clientUploadPrepare(param.md5);
        let url = await this.alioss.path2url(path);
        return {success: true, url: url, headers: headers};
    }

    @Handle('/upload/finished')
    static async uploadFinishedHandler(param: any) {
        // just do nothing.
        param.success = true;
        return param;
    }

}
