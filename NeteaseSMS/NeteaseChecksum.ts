/**
 * Created by Weizehua on 2017/1/21.
 */

import {Crypto} from "node-ts-crypto-promise"

export class NeteaseChecksum {
    static async generate(appSecret:string, currentTime:Date, nonce:string): Promise<string> {
        return (await Crypto.hash(appSecret + nonce + currentTime, 'SHA1')).toString('hex');
    }
    static generateNonce(length:number = 128): string {
        return Crypto.randomBytes(Math.floor(length/3*2)).toString('base64');
    }
}
