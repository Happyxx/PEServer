/**
 * Created by Weizehua on 2017/1/23.
 */
import {Singleton, FactorOf} from "ts.di";
let geetestCtor: any = <any>require('./gt-sdk');

@Singleton()
export class Geetest {
    private sdk: Geetest;

    constructor(config: any) {
        this.sdk = new geetestCtor(config);
    }

    register(): Promise<any> {
        return this.sdk.register();
    }

    validate(param: any): Promise<any> {
        return this.sdk.validate(param);
    }
}
